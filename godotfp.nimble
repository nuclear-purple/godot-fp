# Package

version       = "0.1"
author        = "nuclear-purple"
description   = "An attempt to add fixed-point physics and vectors to Godot."
license       = "MIT"
srcDir        = "src"
skipDirs      = @["godot", "collisionOld"]

# Dependencies

requires "nim >= 1.4.8"
requires "godot >= 0.8.5"

task test, "Runs all tests":
  exec "nim c -r tests/collision.nim"
  exec "nim c -r tests/rotation.nim"
  exec "nim c -r tests/sqrt.nim"
  exec "nim c -r tests/trig.nim"
  exec "nim c -r tests/vec3q.nim"
  exec "nim c -r tests/testfp.nim"
  exec "nim c -r tests/float_conversion.nim"

task docs, "Writes the documentation files":
  exec "nim c -r docs/docgen.nim"
