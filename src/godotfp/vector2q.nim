# fpvector.nim
# Copied and modified from https://github.com/pragmagic/godot-nim under MIT License

## Fixed Point Vector library designed for use with Godot game engine.

import fpn, hashes

const Epsilon = 0b100001 # Margin of error for comparing fp numbers.

# Define the main type and declaration functions
type
  Vector2Q* = object
    x*, y*: FixedPointN

func vec2Q*(): Vector2Q {.inline.} =
  ## Returns an empty two-dimensional vector
  ## The syntax is made to resemble godot-nim's vector syntax.
  Vector2Q()

func vec2Q*(x, y: FixedPointN): Vector2Q {.inline.} =
  ## Returns a new Vector2Q object from received fixed point numbers.
  Vector2Q(x: x, y: y, z: z)

func vec2Q*(x, y: int): Vector2Q {.inline.} =
  ## Returns a new Vector2Q object from received integers.
  var xFP, yFP: FixedPointN
  xFP = newFix(x)
  yFP = newFix(y)
  Vector2Q(x: xFP, y: yFP)

# Operators
proc `$`*(self: Vector2Q): string {.inline.} =
  result = newStringOfCap(40)
  result.add('(')
  result.add($self.x)
  result.add(", ")
  result.add($self.y)
  result.add(")")

# === updated up to this point ===

proc hash*(self: Vector2Q): Hash {.inline.} =
  !$(self.x.hash() !& self.y.hash() !& self.z.hash())

proc `+`*(a, b: Vector2Q): Vector2Q {.inline.} =
  result.x = a.x + b.x
  result.y = a.y + b.y
  result.z = a.z + b.z

proc `+=`*(a: var Vector2Q, b: Vector2Q) {.inline.} =
  a.x += b.x
  a.y += b.y
  a.z += b.z

proc `-`*(a, b: Vector2Q): Vector2Q {.inline.} =
  result.x = a.x - b.x
  result.y = a.y - b.y
  result.z = a.z - b.z

proc `-=`*(a: var Vector2Q, b: Vector2Q) {.inline.} =
  a.x -= b.x
  a.y -= b.y
  a.z -= b.z

proc `*`*(a, b: Vector2Q): Vector2Q {.inline.} =
  result.x = a.x * b.x
  result.y = a.y * b.y
  result.z = a.z * b.z

proc `*=`*(a: var Vector2Q, b: Vector2Q) {.inline.}=
  a.x *= b.x
  a.y *= b.y
  a.z *= b.z

proc `*`*(a: Vector2Q; b: SomeInteger): Vector2Q {.inline.} =
  result.x = a.x * b
  result.y = a.y * b
  result.z = a.z * b

proc `*`*(b: SomeInteger; a: Vector2Q): Vector2Q {.inline.} =
  a * b

proc `*=`*(a: var Vector2Q; b: SomeInteger) {.inline.} =
  a.x *= b
  a.y *= b
  a.z *= b

proc `*`*(a: Vector2Q; b: FixedPointN): Vector2Q {.inline.} =
  result.x = a.x * b
  result.y = a.y * b
  result.z = a.z * b

proc `*`*(b: FixedPointN; a: Vector2Q): Vector2Q {.inline.} =
  a * b

proc `*=`*(a: var Vector2Q; b: FixedPointN) {.inline.} =
  a.x *= b
  a.y *= b
  a.z *= b

proc `/`*(a, b: Vector2Q): Vector2Q =
  result.x = a.x / b.x
  result.y = a.y / b.y
  result.z = a.z / b.z

proc `/=`*(a: var Vector2Q; b: Vector2Q) {.inline.} =
  a.x /= b.x
  a.y /= b.y
  a.z /= b.z

proc `/`*(a: Vector2Q; b: SomeInteger): Vector2Q =
  result.x = a.x / b
  result.y = a.y / b
  result.z = a.z / b

proc `/=`*(a: var Vector2Q; b: SomeInteger) {.inline.} =
  a.x /= b
  a.y /= b
  a.z /= b

proc `/`*(a: Vector2Q; b: FixedPointN): Vector2Q =
  result.x = a.x / b
  result.y = a.y / b
  result.z = a.z / b

proc `/=`*(a: var Vector2Q; b: FixedPointN) {.inline.} =
  a.x /= b
  a.y /= b
  a.z /= b

proc `==`*(a, b: Vector2Q): bool {.inline.} =
  a.x == b.x and a.y == b.y and a.z == b.z

proc `<`*(a, b: Vector2Q): bool =
  if a.x == b.x:
    if a.y == b.y:
      return a.z < b.z
    return a.y < b.y
  return a.x < b.x

proc `-`*(self: Vector2Q): Vector2Q =
  result.x = -self.x
  result.y = -self.y
  result.z = -self.z

proc `[]`*(self: Vector2Q, idx: range[0..2]): FixedPointN {.inline.} =
  cast[array[3, FixedPointN]](self)[idx]

proc `[]`*(self: var Vector2Q, idx: range[0..2]): var FixedPointN {.inline.} =
  cast[ptr array[3, FixedPointN]](addr self)[][idx]

proc `[]=`*(self: var Vector2Q, idx: range[0..2],
            val: FixedPointN) {.inline.} =
  case idx:
  of 0: self.x = val
  of 1: self.y = val
  of 2: self.z = val

# Main methods
proc minAxis*(self: Vector2Q): int {.inline.} =
  if self.x < self.y:
    if self.x < self.z: 0 else: 2
  else:
    if self.y < self.z: 1 else: 2

proc maxAxis*(self: Vector2Q): int {.inline.} =
  if self.x < self.y:
    if self.y < self.z: 2 else: 1
  else:
    if self.x < self.z: 2 else: 0

proc length*(self: Vector2Q): FixedPointN {.inline.} =
  let x2 = self.x * self.x
  let y2 = self.y * self.y
  let z2 = self.z * self.z

  result = sqrt(x2 + y2 + z2)

proc lengthSquared*(self: Vector2Q): FixedPointN {.inline.} =
  let x2 = self.x * self.x
  let y2 = self.y * self.y
  let z2 = self.z * self.z

  result = x2 + y2 + z2

proc lengthSquaredInt*(self: Vector2Q): int64 {.inline.} =
  ## Returns the squared length of a vector as an int64.
  ## Use this with FPNs shorter than i64 in order to prevent overflows.
  let xInt = int64(self.x.rawData)
  let yInt = int64(self.y.rawData)
  let zInt = int64(self.z.rawData)

  result = (xInt * xInt + yInt * yInt + zInt * zInt) shr self.x.rawFracBits

proc normalize*(self: var Vector2Q) {.inline.} =
  let len = self.length()
  if len.isZero():
    self.x = newFix(0)
    self.y = newFix(0)
    self.z = newFix(0)
  else:
    self.x /= len
    self.y /= len
    self.z /= len

proc normalized*(self: Vector2Q): Vector2Q {.inline.} =
  result = self
  result.normalize()

proc isNormalized*(self: Vector2Q): bool {.inline.} =
  self.lengthSquared().isEqualApprox(newFix(1))

proc zero*(self: var Vector2Q) {.inline.} =
  self.x = self.x.zero
  self.y = self.y.zero
  self.z = self.z.zero

proc inverse*(self: Vector2Q): Vector2Q {.inline.} =
  vec2Q(self.x.one / self.x, self.y.one / self.y, self.z.one / self.z)

proc cross*(vecA, vecB: Vector2Q): Vector2Q {.inline.} =
  vec2Q(
    vecA.y * vecB.z - vecA.z * vecB.y,
    vecA.z * vecB.x - vecA.x * vecB.z,
    vecA.x * vecB.y - vecA.y * vecB.x)

proc dot*(vecA, vecB: Vector2Q): FixedPointN {.inline.} =
  vecA.x * vecB.x + vecA.y * vecB.y + vecA.z * vecB.z

proc dotInt*(vecA, vecB: Vector2Q): int64 {.inline.} =
  (int64(vecA.x.rawData) * int64(vecB.x.rawData) +
   int64(vecA.y.rawData) * int64(vecB.y.rawData) +
   int64(vecA.z.rawData) * int64(vecB.z.rawData)) shr vecA.x.rawFracBits

proc abs*(self: Vector2Q): Vector2Q {.inline.} =
  vec2Q(ovAbs(self.x), ovAbs(self.y), ovAbs(self.z))

proc sign*(self: Vector2Q): Vector2Q {.inline.} =
  vec2Q(sign(self.x), sign(self.y), sign(self.z))

proc floor*(self: Vector2Q): Vector2Q {.inline.} =
  vec2Q(floor(self.x), floor(self.y), floor(self.z))

proc ceil*(self: Vector2Q): Vector2Q {.inline.} =
  vec2Q(ceil(self.x), ceil(self.y), ceil(self.z))

proc lerp*(self: Vector2Q, other: Vector2Q, t: FixedPointN): Vector2Q {.inline.} =
  vec2Q(
    self.x + t * (other.x - self.x),
    self.y + t * (other.y - self.y),
    self.z + t * (other.z - self.z)
  )

proc distanceTo*(self, other: Vector2Q): FixedPointN {.inline.} =
  (other - self).length()

proc distanceSquaredTo*(self, other: Vector2Q): FixedPointN {.inline.} =
  (other - self).lengthSquared()

proc distanceSquaredInt*(self, other: Vector2Q): int64 {.inline.} =
  ## Returns the squared distance between two vectors as an int64.
  ## Use this with FPNs shorter than i64 in order to prevent overflows.
  (other - self).lengthSquaredInt()

# Necessary for fixed point math

proc isEqualApprox*(v1, v2: Vector2Q, margin = Epsilon): bool {.inline.} =
  abs(v1.x - v2.x).rawData < margin and
  abs(v1.y - v2.y).rawData < margin and
  abs(v1.z - v2.z).rawData < margin

# Angle and rotation functions

proc angleTo*(self, other: Vector2Q): FixedPointN {.inline.} =
  fparctan2(self.cross(other).length(), self.dot(other))

proc xRotated*(self: Vector2Q, angle: FixedPointN): Vector2Q {.inline.} =
  let c = fpcos(angle)
  let s = fpsin(angle)
  vec2Q(self.x,
        c * self.y - s * self.z,
        s * self.y + c * self.z)

proc yRotated*(self: Vector2Q, angle: FixedPointN): Vector2Q {.inline.} =
  let c = fpcos(angle)
  let s = fpsin(angle)
  vec2Q(c * self.x + s * self.z,
        self.y,
        -s * self.x + c * self.z)

proc zRotated*(self: Vector2Q, angle: FixedPointN): Vector2Q {.inline.} =
  let c = fpcos(angle)
  let s = fpsin(angle)
  vec2Q(c * self.x - s * self.y,
        s * self.x + c * self.y,
        self.z)

# Movement functions

proc slide*(self, n: Vector2Q): Vector2Q {.inline.} =
  ## Returns the component of the vector along a plane defined by the given normal.
  ## n must be a normalized vector.
  assert(n.isNormalized())
  result = self - n * self.dot(n)

proc reflect*(self, n: Vector2Q): Vector2Q {.inline.} =
  ## Returns this vector reflected from a plane defined by the given normal.
  ## n must be a normalized vector.
  assert(n.isNormalized())
  result = 2 * n * self.dot(n) - self

proc bounce*(self, n: Vector2Q): Vector2Q {.inline.} =
  ## Returns the vector "bounced off" from a plane defined by the given normal.
  -self.reflect(n)

proc snap*(self: var Vector2Q, other: Vector2Q) =
  self.x = stepify(self.x, other.x)
  self.y = stepify(self.y, other.y)
  self.z = stepify(self.z, other.z)

proc snapped*(self: Vector2Q, step: Vector2Q): Vector2Q =
  ## Returns this vector with each component snapped to the nearest multiple of step.
  ## This can also be used to round to an arbitrary number of decimals.
  result = self
  result.snap(step)
