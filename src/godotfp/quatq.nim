# Copied and modified from https://github.com/pragmagic/godot-nim under MIT License
# Contains code adapted from https://github.com/mattdesl/quat-slerp/ under MIT License

import hashes, fpn, vector3q

type QuatQ* = object
  w*: FixedPointN
  x*: FixedPointN
  y*: FixedPointN
  z*: FixedPointN

proc initQuatQ*(x, y, z, w: FixedPointN): QuatQ {.inline.} =
  result = QuatQ(
    w: w,
    x: x,
    y: y,
    z: z
  )

proc initQuatQ*(axis: Vector3q; angle: FixedPointN): QuatQ {.inline.} =
  assert axis.isNormalized
  let theta = angle/2
  result = QuatQ(
    w: fpcos(theta),
    x: fpsin(theta) * axis.x,
    y: fpsin(theta) * axis.y,
    z: fpsin(theta) * axis.z
  )

proc fromVec3Q*(vec: Vector3Q): QuatQ {.inline.} =
  QuatQ(x: vec.x,
        y: vec.y,
        z: vec.z)

proc toVec3Q*(quat: QuatQ): Vector3Q {.inline.} =
  Vector3Q(x: quat.x,
           y: quat.y,
           z: quat.z)

proc `$`*(self: QuatQ): string {.inline.} =
  return '(' & $self.w & ", " &
               $self.x & ", " &
               $self.y & ", " &
               $self.z & ')'

proc hash*(self: QuatQ): Hash {.inline.} =
  !$(self.x.hash() !& self.y.hash() !& self.z.hash() !& self.w.hash())

proc `+`*(a, b: QuatQ): QuatQ {.inline.} =
  result.w = a.w + b.w
  result.x = a.x + b.x
  result.y = a.y + b.y
  result.z = a.z + b.z

proc `+=`*(a: var QuatQ, b: QuatQ) {.inline.} =
  a = a + b

proc `-`*(a, b: QuatQ): QuatQ {.inline.} =
  result.w = a.w - b.w
  result.x = a.x - b.x
  result.y = a.y - b.y
  result.z = a.z - b.z

proc `-=`* (a: var QuatQ, b: QuatQ) {.inline.} =
  a = a - b

proc `*`*(a: QuatQ, b: FixedPointN): QuatQ {.inline.} =
  result.w = a.w * b
  result.x = a.x * b
  result.y = a.y * b
  result.z = a.z * b

proc `*`*(a: QuatQ, b: SomeInteger): QuatQ {.inline.} =
  result.w = a.w * b
  result.x = a.x * b
  result.y = a.y * b
  result.z = a.z * b

proc `*=`*(a: var QuatQ, b: FixedPointN) {.inline.} =
  a = a * b

proc `*=`*(a: var QuatQ, b: SomeInteger) {.inline.} =
  a = a * b

proc `/`*(a: QuatQ; b: FixedPointN): QuatQ {.inline.} =
  result.w = a.w / b
  result.x = a.x / b
  result.y = a.y / b
  result.z = a.z / b

proc `/`*(a: QuatQ; b: SomeInteger): QuatQ {.inline.} =
  result.w = a.w / b
  result.x = a.x / b
  result.y = a.y / b
  result.z = a.z / b

proc `/=`*(self: var QuatQ, b: FixedPointN) {.inline.} =
  self = self / b

proc `/=`*(self: var QuatQ, b: SomeInteger) {.inline.} =
  self = self / b

proc `==`*(a, b: QuatQ): bool {.inline.} =
  a.w == b.w and a.x == b.x and a.y == b.y and a.z == b.z

proc `-`*(self: QuatQ): QuatQ =
  result.w = -self.w
  result.x = -self.x
  result.y = -self.y
  result.z = -self.z

proc `[]`*(self: QuatQ, idx: range[0..3]): FixedPointN {.inline.} =
  cast[array[4, FixedPointN]](self)[idx]

proc `[]`*(self: var QuatQ, idx: range[0..3]): var FixedPointN {.inline.} =
  cast[ptr array[4, FixedPointN]](addr self)[][idx]

proc `[]=`*(self: var QuatQ, idx: range[0..3],
            val: FixedPointN) {.inline.} =
  case idx:
  of 0: self.w = val
  of 1: self.x = val
  of 2: self.y = val
  of 3: self.z = val

proc length*(self: QuatQ): FixedPointN {.inline.} =
  let w2 = self.w * self.w
  let x2 = self.x * self.x
  let y2 = self.y * self.y
  let z2 = self.z * self.z

  result = sqrt(w2 + x2 + y2 + z2)

proc lengthSquared*(self: QuatQ): FixedPointN {.inline.} =
  let w2 = self.w * self.w
  let x2 = self.x * self.x
  let y2 = self.y * self.y
  let z2 = self.z * self.z

  result = w2 + x2 + y2 + z2

proc normalize*(self: var QuatQ) {.inline.} =
  let len = self.length()
  if len.isZero():
    self.w = self.w.zero
    self.x = self.w.zero
    self.y = self.w.zero
    self.z = self.w.zero
  else:
    self.w /= len
    self.x /= len
    self.y /= len
    self.z /= len

proc normalized*(self: QuatQ): QuatQ {.inline.} =
  result = self
  result.normalize()

proc isNormalized*(self: QuatQ): bool {.inline.} =
  self.lengthSquared().isEqualApprox(self.w.one)

proc invert*(self: var QuatQ) {.inline.} =
  self.x = -self.x
  self.y = -self.y
  self.z = -self.z
  let lenSq = self.lengthSquared
  if not lenSq.isEqualApprox(self.w.one):
    self /= lenSq

proc inverse*(self: QuatQ): QuatQ {.inline.} =
  result = self
  result.invert()

proc inverseUnit(self: QuatQ): QuatQ {.inline.} =
  assert self.isNormalized
  result.w = self.w
  result.x = -self.x
  result.y = -self.y
  result.z = -self.z

proc dot*(a, b: QuatQ): FixedPointN {.inline.} =
  a.w * b.w + a.x * b.x + a.y * b.y + a.z * b.z

proc hamilton*(a, b: QuatQ): QuatQ {.inline.} =
  result.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z
  result.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y
  result.y = a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x
  result.z = a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w

proc xform*(q: QuatQ; v: Vector3q): Vector3q {.inline.} =
  ## Returns the the vector "v" rotated by the rotation quaternion "q"
  # rotation = Q V Q^-1
  assert(q.isNormalized)
  result = hamilton(hamilton(q, fromVec3Q(v)), inverseUnit(q)).toVec3Q

#[
  proc slerp*(a: QuatQ; b: QuatQ; t: FixedPointN): QuatQ {.inline.} = # TODO
  ## Returns the spheric interpolation between quats a and b by factor t
  const Epsilon = newFix(1, 16) + t.one:
  var b2 = b

  # Calculate the cosine
  let cosOmega = dot(a, b)
  # Check if the cosine is negative
  if cosOmega.isNeg:
    cosOmega = -cosOmega
    b2 = -b2

  if cosOmega > Epsilon:
    # standard slerp
    let omega  = fpacos(cosom) # TODO: implement acos
    let sinOmega  = fpsin(omega)
    scaleA = fpsin((t.one - t) * omega) / sinOmega
    scaleB = fpsin(t * omega) / sinOmega
  else:
    # Do a linear interpolation, since they're very close
    scaleA = 1.0 - t
    scaleB = t

  result = scaleA * a + scaleB * b2
]#

#[
proc slerpni*(self: QuatQ; b: QuatQ; t: FixedPointN): QuatQ {.inline.} = # TODO
  getGDNativeAPI().quatSlerpni(self, b, t).toQuat()
]#

#[
proc cubicSlerp*(self, b, preA, postB: QuatQ;
  t: FixedPointN): QuatQ {.inline.} = # TODO
  getGDNativeAPI().quatCubicSlerp(self, b, preA, postB, t).toQuat()
]#
