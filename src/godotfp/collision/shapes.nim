# shapes.nim

import ../vector3q
import ../fpn

type
  ShapeKind* = enum
    SphereShape,
    CapsuleShape,
    CylinderShape

  StageColliderKind* = enum
    RectangleShape,
    BoxShape

  HitShape* = object
    translation*: Vector3Q # Parent object's position
    yRotation*, radius*: FixedPointN
    isDisabled*: bool
    case kind*: ShapeKind

    of SphereShape:
      ## Sphere shape. It is defined by its radius and position in 3D space.
      center*: Vector3Q # Position of the center in relation to the parent's position

    of CapsuleShape:
      ## Capsule shape. It is defined by its radius and two sphere centre positions.
      center1*, center2*: Vector3Q

    of CylinderShape:
      ## y axis-aligned cylinder. It has a base position, a radius and a height.
      ## Cylinder collisions can be highly complex, but by limiting them ...
      ## ... to the y-axis, they become much simpler and easy to compute.
      base*: Vector3Q
      height*: FixedPointN

  StageCollider* = object
    # type for colliders that won't be part of a character's hit/hurtboxes
    origin*, normal*: Vector3Q
    isDisabled*: bool
    case kind*: StageColliderKind

    of RectangleShape:
      ## A rectangle is defined by a vertex P0 (origin)
      ## and two vectors pointing from P0 to P1 and P3 (the other vertexes).
      e1*, e2*: Vector3Q # Vectors pointing from P0 to P1/P2.
    of BoxShape:
      ## A box shape is defined by a center (origin), three half widths,
      ## And a rotation around the vertical axis.
      hWidth*: Vector3Q
      yRotation*: FixedPointN

    # TODO: possibly add OBB and triangles.

# Declarators
const emptyVec = vec3Q()
const up = vec3Q(0, 1, 0)
const zero = newFix()

proc hitSphere*(radius: FixedPointN, center: Vector3Q): HitShape =
  HitShape(kind: SphereShape, radius: radius, center: center)

proc hitCapsule*(radius: FixedPointN, center1, center2: Vector3Q): HitShape =
  HitShape(kind: CapsuleShape, radius: radius, center1: center1, center2: center2)

proc hitCylinder*(radius, height: FixedPointN, base: Vector3Q): HitShape =
  HitShape(kind: CylinderShape, radius: radius, base: base, height: height)

proc rectCollider*(P0, e1, e2: Vector3Q, normal=up, isDisabled=false): StageCollider = # TODO: clean these up
  StageCollider(kind:RectangleShape, origin:P0, e1:e1, e2:e2, normal:normal, isDisabled:isDisabled)

proc boxCollider*(center, hWidth: Vector3Q, yRotation=zero, normal=up, isDisabled=false): StageCollider =
  StageCollider(kind:BoxShape, origin:center, hWidth:hWidth, yRotation:yRotation, normal:normal, isDisabled:isDisabled)

# Sphere getters

proc centerTr*(self: HitShape): Vector3Q {.inline.} =
  ## Returns the sphere's center relative to its translation and rotation.
  self.center.yRotated(self.yRotation) + self.translation

# Capsule getters

proc center1Tr*(self: HitShape): Vector3Q {.inline.} =
  ## Returns the capsule's first center relative to its translation and rotation.
  self.center1.yRotated(self.yRotation) + self.translation

proc center2Tr*(self: HitShape): Vector3Q {.inline.} =
  ## Returns the capsule's second center relative to its translation and rotation.
  self.center2.yRotated(self.yRotation) + self.translation

# Cylinder getters

proc baseTr*(self: HitShape): Vector3Q {.inline.} =
  ## Returns the cylinder's base relative to its translation and rotation.
  self.base.yRotated(self.yRotation) + self.translation

# Rectangle getters

proc P0*(self: StageCollider): Vector3Q {.inline.} = self.origin

# Box getters

proc center*(self: StageCollider): Vector3Q {.inline.} = self.origin

# Setters

proc disable*(self: var HitShape) =
  self.isDisabled = true

proc enable*(self: var HitShape) =
  self.isDisabled = false
