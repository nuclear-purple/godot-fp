# collision.nim

## Defines procs for collision detection

import shapes
import ../vector3q
import ../fpn

# Utility procs

proc saturate(t: FixedPointN): FixedPointN =
  min(max(t, t.zero), t.one)

proc clamp(v, vmin, vmax: Vector3Q): Vector3Q =
  result.x = min(max(v.x, vmin.x), vmax.x)
  result.y = min(max(v.y, vmin.y), vmax.y)
  result.z = min(max(v.z, vmin.z), vmax.z)

proc closestPointOnSegment(A, B, point: Vector3Q): Vector3Q =
  ## Returns the point on the segment AB that is closest to the given point.
  let AB = B - A # Vector pointing from A to B
  let length = AB.lengthSquared
  if length.isZero: return A # Check if the segment has length zero
  return A + AB * saturate(dot(point - A, AB) / length)

proc segPointDistance(A, B, point: Vector3Q): FixedPointN =
  ## Returns the smallest distance between a line segment and a point.
  let AB = B - A
  let length = AB.lengthSquared
  if length.isZero: return length # Check if the segment has length zero
  let closest = A + AB * saturate(dot(point - A, AB) / length)
  return distanceSquaredTo(closest, point)

proc segSegDistance(A, B, C, D: Vector3Q): array[3, FixedPointN] =
  ## Returns the smallest distance between two line segments: (A -> B) and (C -> D).
  ## As well as the parameters t and s.
  var t, s: FixedPointN # The "lerp ammount" between A & B, and C & D
  let frac = A.x.fracBits
  const Epsilon = 100 # margin of error for determining if the segs are parallel
  let AB = B - A # Vector that points from A to B
  let CD = D - C
  let ABsquared = BiggestInt(dot(AB, AB).rawData)
  assert ABsquared != 0 # the AB segment is actually a point
  let CDsquared = BiggestInt(dot(CD, CD).rawData)
  assert CDsquared != 0 # the CD segment is actually a point
  let dotABCD = BiggestInt(dot(AB, CD).rawData)
  let det = (ABsquared * CDsquared - dotABCD * dotABCD) shr frac

  let CA = A - C # vector between segment bases
  let v = BiggestInt(dot(AB, CA).rawData)
  let w = BiggestInt(dot(CD, CA).rawData)

  if det < Epsilon: # That means they are parallel.
    # Compare both seg "bottoms" to their perpendicular point on the other seg.
    t.setData(-v div ABsquared)
    s.setData(w div CDsquared)
    let distS0 = distanceSquaredTo(A + saturate(t) * AB, C)
    let distT0 = distanceSquaredTo(A, C + saturate(s) * CD)
    result[0] = min(distS0, distT0)
    if result[0] == distS0:
      result[1] = t
      result[2] = s.zero
    else:
      result[1] = t.zero
      result[2] = s

  else:
    t.setData((dotABCD * w - CDsquared * v) div det)
    s.setData((ABsquared * w - dotABCD * v) div det)

    result = [distanceSquaredTo(A + saturate(t) * AB, C + saturate(s) * CD), t, s]

proc rectPointDistance(P, e1, e2, Q: Vector3Q): FixedPointN =
  ## Returns the smallest distance between the point Q ...
  ## ... and a rectangle defined by a vertex P and two orthogonal vectors e1 and e2
  # TODO: use int64 like the segSegDistance proc, otherwise it will overflow.
  const zero = newFix(0)
  var d = Q - P # Vector pointing from the point Q to one of the vertices.

  # Project the point onto the plane and test
  let s = dot(e1, d)
  if s > zero:
    let dot1 = dot(e1, e1)
    if s < dot1: # If it's between P0 and P1
      d = d - (s / dot1) * e1
    else: # If it's further than P1 on the first coordinate
      d = d - e1

  let t = dot(e2, d)
  if t > zero:
    let dot2 = dot(e2, e2)
    if t < dot2:
      d = d - (t / dot2) * e2
    else:
      d = d - e2

  return dot(d, d)

# Collision procs

proc sphereToSphere(a, b: HitShape): bool =
  ## Sphere to sphere collision
  ## Just check if the distance between both centres is smaller than the
  ## sum of both radii. Very computationally efficient
  distanceSquaredInt(a.centerTr, b.centerTr) < squareInt(a.radius + b.radius)

proc sphereToCapsule(sph, cap: HitShape): bool =
  ## Sphere to capsule collision
  ## About 1.5 times slower than sphere-sphere.
  # Checks to see if the sphere is "after" or "before" the capsule's edges.
  # If it is, then performs collision detection with the closest center.
  # If not, then measures the distance from the sphere to the line segment AB.
  let capC1 = cap.center1Tr
  let capC2 = cap.center2Tr
  let sphC = sph.centerTr
  let AB = capC2 - capC1
  let AC = sphC - capC1
  let BC = sphC - capC2
  let dotABAC = dotInt(AB, AC)

  if dotABAC <= 0:
    return (sphC - capC1).lengthSquaredInt < square(sph.radius + cap.radius).rawData
  if dotInt(-AB, BC) <= 0:
    return (sphC - capC2).lengthSquaredInt < square(sph.radius + cap.radius).rawData

  let distanceSq = AC.lengthSquaredInt - dotABAC * dotABAC div AB.lengthSquaredInt
  return distanceSq < squareInt(sph.radius + cap.radius)

proc sphereToCylinder(sph, cyl: HitShape): bool =
  ## Sphere to cylinder collision.
  ## Treats it as a sphere-to-capsule if the sphere's Y is between the capsule's Ys.
  ## Otherwise, create a 2D cut from the sphere and compare that to the cylinder.
  let cylBase = cyl.baseTr # transforms the cylinder
  let sphCenter = sph.centerTr # transforms the sphere
  let bottom = cylBase.y
  let top = bottom + cyl.height

  var newRadius = sph.radius

  if sphCenter.y + sph.radius <= bottom or sphCenter.y - sph.radius >= top:
    return false
  elif sphCenter.y > top:
    newRadius = sqrt(square(sph.radius) - square(sphCenter.y - top))
  elif sphCenter.y < bottom:
    newRadius = sqrt(square(sph.radius) - square(bottom - sphCenter.y))

  return squareInt(cylBase.x - sphCenter.x) + squareInt(cylBase.z - sphCenter.z) < squareInt(newRadius + cyl.radius)

proc capsuleToCapsule(a, b: HitShape): bool =
  ## Capsule to capsule collision.
  ## Compares the shortest distance between the capsules' line segments.
  # TODO: Make this not overflow when the distanceSq is >36k
  segSegDistance(a.center1Tr, a.center2Tr, b.center1Tr, b.center2Tr)[0] < square(a.radius + b.radius)

proc capsuleToCylinder(cap, cyl: HitShape): bool =
  ## Capsule to cylinder collision.
  ## First, treat the cylinder as a capsule and find the distance between
  ## The cylinder's segment and the capsule's.
  ## If the capsule's closest point is above or below the cylinder,
  ## Then do the same thing we did for the sphere.
  # TODO: Make this not overflow when the distanceSq is >36k
  let c1 = cap.center1Tr
  let c2 = cap.center2Tr
  let bottom = cyl.baseTr
  var top = bottom
  top.y += cyl.height
  let d = segSegDistance(c1, c2, bottom, top)
  let t = d[1] # capsule's parameter
  let s = d[2] # cylinder's parameter

  if s < s.zero or s > s.one:
    var newRadius = cap.radius
    var newCenter = c1 + t*(c2 - c1)

    if newCenter.y + cap.radius <= bottom.y or newCenter.y - cap.radius >= top.y:
      return false
    elif newCenter.y >= top.y:
      newRadius = sqrt(square(cap.radius) - square(newCenter.y - top.y))
    elif newCenter.y <= bottom.y:
      newRadius = sqrt(square(cap.radius) - square(bottom.y - newCenter.y))

    return squareInt(bottom.x - newCenter.x) + squareInt(bottom.z - newCenter.z) < squareInt(newRadius + cyl.radius)

  else:
    return d[0] < square(cap.radius + cyl.radius)

  # TODO: optimize this:
  # maybe make a version of segSegDistance that takes into account ...
  # that the cylinder is vertical.

proc cylinderToCylinder(a, b: HitShape): bool =
  ## Cylinder collision. Very simple, since they are axis-aligned.
  ## If one is above the other, they aren't colliding.
  ## Otherwise, treat each of them as a 2D circle and ignore the y coordinate.
  let aBase = a.baseTr
  let bBase = b.baseTr
  var aTop = aBase.y + a.height
  var bTop = bBase.y + b.height

  if not (aTop > bBase.y and bTop > aBase.y): # check if above or below
    return false
  return squareInt(aBase.x - bBase.x) + squareInt(aBase.z - bBase.z) < squareInt(a.radius + b.radius)

proc sphereToRect(a: HitShape, b: StageCollider): bool =
  ## Sphere to rectangle collision.
  # TODO: optimize rectPointDistance to transform from fp to int
  rectPointDistance(b.P0, b.e1, b.e2, a.centerTr) < square(a.radius)

proc sphereToBox(a: HitShape, b: StageCollider): bool =
  ## Sphere to OBB collision.
  # First, transform the sphere's center to be in the box's axis coordinates.
  # This makes the OBB become an AABB in its own axis.
  var centerRotated = a.centerTr - b.origin # From the box's center to the sphere's
  # Now rotate the vector
  centerRotated = centerRotated.yRotated(-b.yRotation)
  # Now clamp the sphere's center to the AABB's limits
  let closestPoint = clamp(centerRotated, -b.hWidth, b.hWidth)
  distanceSquaredInt(centerRotated, closestPoint) < squareInt(a.radius)

proc capsuleToRect(a: HitShape, b: StageCollider): bool = discard
  # TODO? capsule to rect is even more difficult than cap to box,
  # So I'm thinking about giving up on rectangles altogether.

proc capsuleToBox(a: HitShape, b: StageCollider): bool = discard
  # TODO: capsule to box is too difficult lol

proc cylinderToRect(a: HitShape, b: StageCollider): bool = discard

proc cylinderToBox(a: HitShape, b: StageCollider): bool =
  ## Cylinder to box collision.
  let cylBase = a.baseTr
  let cylBottom = cylBase.y
  let cylTop = cylBottom + a.height
  let boxBottom = b.center.y - b.hWidth.y
  let boxTop = b.center.y + b.hWidth.y

  # Check to see if the cylinder is above or below the box.
  if cylBottom >= boxTop or cylTop <= boxBottom:
    return false

  # Otherwise, consider the cylinder as a  circle and test against the box.
  var baseRotated = cylBase - b.origin
  baseRotated = baseRotated.yRotated(-b.yRotation)
  let closestPoint = clamp(baseRotated, -b.hWidth, b.hWidth)
  return distanceSquaredTo(baseRotated, closestPoint) < a.radius

proc isColliding*(a, b: HitShape): bool =
  case a.kind

  of SphereShape:
    case b.kind
    of SphereShape:
      return sphereToSphere(a, b)
    of CapsuleShape:
      return sphereToCapsule(a, b)
    of CylinderShape:
      return sphereToCylinder(a, b)

  of CapsuleShape:
    case b.kind
    of SphereShape:
      return sphereToCapsule(b, a)
    of CapsuleShape:
      return capsuleToCapsule(a, b)
    of CylinderShape:
      return capsuleToCylinder(a, b)

  of CylinderShape:
    case b.kind
    of SphereShape:
      return sphereToCylinder(b, a)
    of CapsuleShape:
      return capsuleToCylinder(b, a)
    of CylinderShape:
      return cylinderToCylinder(a, b)

proc isColliding*(a: HitShape, b: StageCollider): bool =
  case a.kind

  of SphereShape:
    case b.kind
    of RectangleShape:
      return sphereToRect(a, b)
    of BoxShape:
      return sphereToBox(a, b)

  of CapsuleShape:
    case b.kind
    of RectangleShape:
      return capsuleToRect(a, b)
    of BoxShape:
      return capsuleToBox(a, b)

  of CylinderShape:
    case b.kind
    of RectangleShape:
      return cylinderToRect(a, b)
    of BoxShape:
      return cylinderToBox(a, b)

proc isColliding*(b: StageCollider, a: HitShape): bool =
  isColliding(a, b)
