# Copied and modified from https://gitlab.com/lbartoletti/fpn under MIT License

import math

# abs
proc isSafeAbs*(a: SomeInteger): bool =
  ## Returns true if is safe to return the absolute value of `a`
  ##
  ## On signed integers abs(`a`) is safe only if a is not low(`a`)
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##    var a = low(int16)
  ##    echo a ## -32768
  ##    echo isSafeAbs(a) ## false
  ##    echo abs(a) ## OverflowError
  ##
  a != low(typeof(a))

# Negate
proc isSafeNegate*(a: SomeInteger): bool =
  ## Returns true if is safe to return the negative value of `a`
  ##
  ## On signed integers -`a` is safe only if a is not low(`a`)
  ##
  ## See also:
  ##  * `isSafeAbs proc <#isSafeAbs,SomeInteger>`_
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##    var a = low(int16)
  ##    echo a ## -32768
  ##    echo isSafeNegate(a) ## false
  ##    echo -a ## OverflowError
  ##
  a != low(typeof(a))

# Addition
proc isUnderflowAdd*(a, b: SomeInteger): bool =
  ## Returns true if `a` + `b` produces an underflow error
  b < 0 and a < low(typeof(a)) - b

proc isOverflowAdd*(a, b: SomeInteger): bool =
  ## Returns true if `a` + `b` produces an overflow error
  b > 0 and a > high(typeof(a)) - b

proc isSafeAdd*(a, b: SomeInteger): bool =
  ## Returns true if `a` + `b` is safe (no under- or overflow error)
  not isUnderflowAdd(a, b) and not isOverflowAdd(a, b)

proc ovAdd*(a, b: SomeInteger): SomeInteger =
  var pb = b
  result = a
  while pb != 0:
      var carry = result and pb
      result = result xor pb
      pb = carry shl 1

# Substraction
proc isUnderflowSub*(a, b: SomeInteger): bool =
  # Returns true if `a` - `b` produces an underflow error
  b > 0 and a < low(typeof(a)) + b

proc isOverflowSub*(a, b: SomeInteger): bool =
  # Returns true if `a` - `b` produces an overflow error
  b < 0 and a > high(typeof(a)) + b

proc isSafeSub*(a, b: SomeInteger): bool =
  ## Returns true if `a` - `b` is safe (no under- or overflow error)
  not isUnderflowSub(a, b) and not isOverflowSub(a, b)

proc ovSub*(a, b: SomeInteger): SomeInteger =
  var pb = b
  result = a
  while pb != 0:
    var borrow = (not result) and pb
    result = result xor pb
    pb = borrow shl 1

# Multiplication

proc count_int_divide*(x: SomeInteger) : SomeInteger =
  var n = x
  while ( n != 0 ):
    result += 1
    n = n div 10

proc karatsuba*(x, y: BiggestInt) : BiggestInt =
  if (x < 10 and y < 10):
    return x*y

  var n = max(count_int_divide(x), count_int_divide(y))
  var m = n div 2

  var div_factor = 10 ^ m
  var x_H = x div div_factor
  var x_L = x mod div_factor

  var y_H = y div div_factor
  var y_L = y mod div_factor

  var a = karatsuba(x_H, y_H)
  var d = karatsuba(x_L, y_L)
  var e = karatsuba(x_H + x_L, y_H + y_L) - a - d

  (a * (10 ^ (m * 2) ) + e * (10 ^ m) + d)

# Division
proc isOverflowDiv*(a, b: SomeInteger) : bool =
  # Returns true if `a` / `b` produces an overflow error
  (a == low(typeof(a)) and b == -1)

proc isUnderflowDiv*(a, b: SomeInteger) : bool =
  # Returns true if `a` / `b` produces an underflow error...
  # Wait... no. Returns false
  false

proc isSafeDiv*(a, b: SomeInteger) : bool =
  ## Returns true if `a` / `b` is safe (no under- or overflow error) and b != 0
  b != 0 and not isOverflowDiv(a, b)

proc isqrt*(a: SomeInteger): SomeInteger =
  ## https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Example_3
  ## Square root binary 2
  var bit: typeof(a) = 1 shl (sizeof(a)*8 - 2)
  var num: typeof(a) = a

  while (bit > num):
    bit = bit shr 2

    while (bit != 0):
      if (num >= result + bit):
        num -= result + bit
        result = (result shr 1) + bit
      else:
        result = result shr 1
      bit = bit shr 2

