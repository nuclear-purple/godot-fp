# vec_conversion.nim

## Defines procs for converting fixed vectors to float vectors and vice-versa

# if it complains about "GC_step", run it with nim c -r -d:useRealtimeGC

import godot
import fpn, vector3q

proc toFloat*(v: Vector3q): Vector3 {.inline.} =
  ## Converts a fixed point vector to a float vector and returns it
  result.x = v.x.toFloat
  result.y = v.y.toFloat
  result.z = v.z.toFloat

proc toFixed*(v: Vector3): Vector3q {.inline.} =
  ## Converts a fixed point vector to a float vector and returns it
  result.x = v.x.fpn
  result.y = v.y.fpn
  result.z = v.z.fpn

