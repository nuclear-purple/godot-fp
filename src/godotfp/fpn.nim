# Copied and modified from https://gitlab.com/lbartoletti/fpn under MIT License

import fpn/fpn
import fpn/fpmath
export fpn
export fpmath

# Alter the following statements as necessary to set which type of fpn is used:
genQM_N(newFix, 16, fixedPoint32)
type FixedPointN* = fixedPoint32[16] # Default: fixedPoint32[16]

proc fpn*(i: SomeInteger): FixedPointN =
  ## Pair of procs used to convert an integer or float to a fixed point number.
  ## It is meant to be a replacement for nim's type suffixes, like 'i32.
  ## Example: var a = 5.fpn # assigns a fpn with the value 5.0 to the var 'a'.
  result.fromInt(i)

proc fpn*(f: SomeFloat): FixedPointN =
  ## Converts the float f to a fixed point number and returns it.
  ## See also the above proc `fpn(i: SomeInteger)`
  result.fromFloat(f)
