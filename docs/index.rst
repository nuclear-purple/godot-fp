=============================
Nim bindings for Godot Engine
=============================

:Author: Nuclear Purple
:Version: 0.0.1
:GitLab: `godot-fp <https://gitlab.com/nuclear-purple/godot-fp>`_
:License: MIT

.. contents::

``Godot-fp`` is a physics library that provides a
`fixed-point <https://en.wikipedia.org/wiki/Fixed-point_arithmetic>`_ number system
as well as deterministic collision detection for the
`Godot Game Engine <https://godotengine.org/>`_ using the
`Nim language <https://nim-lang.org/>`_. Nim is an easy to use and fast language that
has a good integration with Godot via `godot-nim <https://github.com/pragmagic/godot-nim/>`_.

Determinism is necessary when creating multiplayer games that use peer-to-peer networking.
In these cases, it's necessary that both players' games run exactly the same, frame by frame.
Unfortunately, Godot's physics system makes this task difficult, because of its reliance on
floating point arithmetic, which is inherently non-deterministic (different processors treat float
operations differently, resulting in slight inaccuracies).

This library aims to use fixed point numbers (a modified version of
Nim's `fpn module <https://gitlab.com/lbartoletti/fpn>`_ to solve that issue and provide an
easy framework for creating P2P games such as fighting games. It focuses on 3D vector math for now,
but 2D vectors will be included in the future.

If you are not familiar with Nim yet, it is recommended to go through the
`official tutorial <https://nim-lang.org/docs/tut1.html>`_. With the release of Godot 4, an
addon will be created to allow this library to be used with gdscript.

Getting Started
===============

Getting Godot
-------------

The library requires any version of Godot 3, which you can get here:
https://godotengine.org/download

Installing Nim
--------------

The library requires Nim version 1.4.0 or newer, which you can get here:
https://nim-lang.org/install.html.

It is recommended to use `choosenim <https://github.com/dom96/choosenim#choosenim>`_,
as it provides an easy way to keep Nim up to date.

Creating a project or adding Godot-fp to an existing one
--------------------------------------------------------

Follow the instructions under https://github.com/pragmagic/godot-nim to include it in your project.
After that, download the `src folder <https://gitlab.com/nuclear-purple/godot-fp/-/tree/main/src>`_
from Gitlab and copy it to your project's src directory, or wherever you keep your Nim scripts.


Documentation
=============

* `Collision <collision.html>`_: Explanations about the collision system.
