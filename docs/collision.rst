=============================
Collision
=============================

.. contents::

This submodule defines 3D shapes to be used as hurt/hitboxes, as well as
procedures to detect their intersection. Use the proc ``isColliding(hitbox, hurtbox)``
between two shapes to detect if they are intersecting.

Hitbox Shapes
=============

Sphere
------

The most simple and efficient of all shapes provided in this module. Spheres are the most
classic and fast way to provide colliders for 3D characters.

They are defined by a fixed point ``radius`` and a vector ``center``, which represents
the center's position in relation to the character's base position. It also has
``translation`` and ``yRotation`` (vertical rotation) parameters, which are not
its own translation and rotation, but are meant to be copied from its parent character's coordinates.

Sphere-sphere and sphere-cylinder collision is very fast, while sphere-capsule has a medium performance.

.. image:: images/sphere.png
    :width: 600px
    :align: center
    :alt: alternate text

Capsule
-------

Capsules, also called swept spheres, are a slightly more complex shape than the other two, but one that can
more accurately represent the shape of a humanoid body than spheres. Compared to spheres, they are slower, but
using capsules allows you to have fewer collision shapes per body, which more than compensates the slight
performance loss.

While a sphere can be thought of as a point with thickness, think of a capsule as line segment with thickness.
You can also imagine it as two spheres and a cylinder connecting them. They are defined by two centers
(their edge spheres) and a radius. They also have ``translation1`` and ``translation2`` parameters, which are
each center's distance to the character's position, as well as a ``yRotation``

Capsule-capsule and capsule-cylinder collisions are tied as the slowest of the bunch, but they're very fast for their
complexity. sphere-capsule has medium speed.

.. image:: images/capsule.png
    :width: 600px
    :align: center
    :alt: alternate text

Cylinder
--------

Cylinders can either be needlessly complex or painfully simple, depending on how you implement them.
This module limits cylinders to vertical (y-aligned) cylinders, because this absurdly simplifies
cylinder-cylinder collision (which is otherwise incredibly slow). Apparently, games like Tekken use
the same vertical cylinder system for their hurtboxes.

A cylinder is defined as a ``base`` vector coordinate, which is the position of the center of its bottom face,
as well as a ``height`` fixed point parameter. It also has ``translation`` and ``yrotation``, just like a sphere.

cylinder-cyliner collision is very fast, perhaps even faster than sphere-sphere. Cylinder-sphere is fast, and cylinder-capsule
is medium-slow.

.. image:: images/cylinder.png
    :width: 600px
    :align: center
    :alt: alternate text


Stage Shapes
============

These shapes were not meant to be used as hitboxes or hurtboxes for your characters. They're simply stage colliders.
As such, there is no function implemented for rect-rect or OBB-OBB collision.

Rectangle
---------

Rect, for short. Rects are simple shapes that can be oriented in whatever direction you want. They're defined by
a top-left vertex, as well as two vectors that define edges. They also have a ``normal`` parameter for defining
in which direction the character should be pushed when they collide with it.


Oriented Bounding Box (OBB)
---------------------------

OBBs are usually a very complex shape, specially in 3D. Luckily for us, this module only supports y-rotated boxes.
That simplifies things a lot for the end user and makes cylinder-box collision easier. If you want to make a ramp, use a rect,
though other types of OBBs might get implemented if there is enough demand for it.


Collision algorithms
====================

Sphere-sphere
-------------
WIP

Sphere-capsule
--------------
WIP

capsule-capsule
---------------
WIP

sphere-cylinder
---------------
WIP

capsule-cylinder
----------------
WIP

cylinder-cylinder
-----------------
WIP

sphere-rect
-----------
WIP

sphere-box
----------
WIP

cylinder-box
------------
WIP

Files in this submodule
=======================

* `shapes.nim <godotfp/collision/shapes.html>`_
* `collision.nim <godotfp/collision/collision.html>`_
