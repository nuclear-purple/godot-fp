# docgen.nim
# parses the rst files into html for the gitlab pages site

import os
from strutils import split

# If it's running from the base godot-fp folder, set it to docs
let currentFolder = getCurrentDir()
if currentFolder.split('/')[^1] != "docs":
  try:
    setCurrentDir(currentFolder & "/docs")
  except OSError:
    echo "This tool must be run from the godot-fp project folder or /docs."
    quit(-1)

# read all .rst files in current directory
var rstFiles: seq[string] = @[]
for file in walkFiles("*"):
  if file[^4 .. ^1] == ".rst":
    rstFiles.add(file)

echo rstFiles

# run the doc for the .nim files
discard execShellCmd("nim doc --project --index:on --git.url:https://gitlab.com/nuclear-purple/godot-fp --git.commit:main --outdir:htmldocs ../src/godotfp.nim")
# TODO: understand why the hell --git.commit:main is not working.
# It's ignoring the branch name for some reason.

for file in rstFiles:
  discard execShellCmd("nim rst2html " & file)

copyDir("images", "htmldocs/images")
