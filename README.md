# Godot Fixed Point

An attempt to provide a framework for working with fixed-point numbers using Godot and Nim.

Since Godot's default physics are not deterministic, this library attempts to enable the creation of games that *need* determinism to work, such as fighting games, as well as most peer-to-peer networking games.

### Documentation

Documentation for this project can be found [here](https://nuclear-purple.gitlab.io/godot-fp/index.html).

### Links to other projects that add deterministic physics to Godot:

+ [Alcatreize](https://github.com/Carbone13/alcatreize-godot/): A deterministic physics engine written in C#, for use with godot-mono.
+ (More to come!)

### Credits

This repository uses code copied and modified from the following repositories under the MIT license:

+ [lbartoletti/fpn](https://gitlab.com/lbartoletti/fpn)
+ [pragmagic/godot-nim](https://github.com/pragmagic/godot-nim)