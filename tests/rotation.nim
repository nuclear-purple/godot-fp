# rotation.nim
# I decided to  make a test file just for rotation because
# it's ultra important in a 3D game. --nuclear

import godotfp, unittest

var Pi = newFix()
Pi = Pi.pi

test "xrotation":
  var v1 = vec3Q(1, 0, 0)
  check v1.xrotated(Pi/2).isEqualApprox(vec3Q(1, 0, 0))
  echo v1.xrotated(Pi/2)

test "yrotation":
  var v1 = vec3Q(1, 0, 0)
  var v2 = v1
  v2.x.fromFloat(0.7071)
  v2.z.fromFloat(-0.7071)
  check v1.yrotated(Pi/2).isEqualApprox(vec3Q(0, 0, -1))
  echo v1.yrotated(Pi/2)
  check v1.yrotated(Pi/4).isEqualApprox(v2)
  echo $v1.yrotated(Pi/4) & $v2
  check v1.yrotated(Pi/4).isNormalized

test "zrotation":
  var v1 = vec3Q(1, 0, 0)
  check v1.zrotated(Pi/2).isEqualApprox(vec3Q(0, 1, 0))
  echo v1.zrotated(Pi/2)
