# trig.nim

import godotfp/fpn
import unittest, times
import std/monotimes

test "benchmark":
  var a, b: fixedPoint32[16]

  #[let time1 = getMonoTime()
  for i in countup(1, 10000):
    b = fpcosTaylor(a)
    a.setData(a.rawData + 1)
  let time2 = getMonoTime()
  let timeTaylor = time2 - time1 ]#

  let time3 = getMonoTime()
  for i in countup(1, 10000):
    b = fpcos(a)
    a.setData(a.rawData + 1)
  let time4 = getMonoTime()
  let timeLut = time4 - time3

  let time5 = getMonoTime()
  for i in countup(1, 10000):
    b = fparctan(a)
    a.setData(a.rawData + 1)
  let time6 = getMonoTime()
  let timeArctan = time6 - time5

  # echo "taylor: ", timeTaylor
  echo "cos:    ", timeLut
  echo "arctan: ", timeArctan

test "cos":
  var a: fixedPoint32[16]
  var w: fixedPoint32[16]
  a = a.pi / 6
  echo fpcos(a)

  w.setData(1)
  let Pi = w.pi
  check fpsin(newFix(0)) == w.zero
  check fpcos(Pi/2 + w).isNeg
  check fpcos(Pi/2) == w.zero

test "arctan":
  var a, b: fixedPoint32[16]
  a = a.one * -2
  b = a.one * 2
  echo fparctan2(a, b)
  check fparctan2(a, b).isEqualApprox(-a.pi / 4)
