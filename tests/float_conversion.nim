# float_conversion.nim

import unittest, godotfp, times, std/monotimes

test "float64":
  var floats = [0.0, 1.0, 3.1416, -9.99, 1000.0, -80000.0]
  var a: fixedPoint32[16]
  var b: fixedPoint32[16]
  for f in floats:
    a.fromFloat(f)
    b.fromFloatOld(f)
    check a.isEqualApprox(b)

test "float32":
  var floats = [0.0 ,1.0'f32, 3.1416'f32, -9.99'f32, 1000.0'f32, -80000.0'f32]
  var a: fixedPoint32[16]
  var b: fixedPoint32[16]
  for f in floats:
    a.fromFloat(f)
    b.fromFloatOld(f)
    # check a == b # use this for better debugging
    check a.isEqualApprox(b)

test "benchmark":

  var a: fixedPoint32[16]
  let time1 = getMonoTime()
  for i in 1..1000000:
    a.fromFloat(float(i))
  let time2 = getMonoTime()
  let timeNew = time2 - time1

  let time3 = getMonoTime()
  for i in 1..1000000:
    a.fromFloatOld(float(i))
  let time4 = getMonoTime()
  let timeOld = time4 - time3

  echo "time (new): " & $timeNew
  echo "time (old): " & $timeOld
