# Test collision edge cases:
# overflow and parallel lines.
# This file won't be included in 'nimble test'. While it's good that the
# collision detection passes these tests, it's not absolutely mandatory.

# The algorithm for parallel capsules is being worked on. I'm trying to find
# a balance between performance and accuracy. --Nuclear

import godotfp, unittest

test "parallel":
  let c1 = hitCapsule(center1 = vec3Q(2, 0, 0), center2 = vec3Q(2, 5, 0), radius = newFix(1))
  let c2 = hitCapsule(center1 = vec3Q(3, 10, 0), center2 = vec3Q(3, 15, 0), radius = newFix(1))
  let c3 = hitCapsule(center1 = vec3Q(5, 2, 0), center2 = vec3Q(5, 7, 0), radius = newFix(1))
  let c4 = hitCapsule(center1 = vec3Q(3, 2, 0), center2 = vec3Q(3, 4, 0), radius = newFix(1))
  # echo segSegDistance(c1.center1, c1.center2, c2.center1, c2.center2)
  check isColliding(c1, c2) == false
  check isColliding(c1, c3) == false
  check isColliding(c1, c4) == true # error: will give out false
  check isColliding(c2, c3) == false
  check isColliding(c2, c4) == false
  check isColliding(c3, c4) == false

test "overflow sphere":
  # tries to run the algorithm with wildly distant spheres.
  # the resulting squared distance will be an integer bigger than i32 or i64.
  let s1 = hitSphere(center = vec3Q(150, 0, 0), radius = newFix(10000))
  let s2 = hitSphere(center = vec3Q(-150, 0, 0), radius = newFix(10000))
  let s3 = hitSphere(center = vec3Q(10000, 10, 0), radius = newFix(10000))
  let s4 = hitSphere(center = vec3Q(-10000, -10, 0), radius = newFix(10000))

  check isColliding(s1, s2) == true
  check isColliding(s3, s4) == false

test "overflow sphere-capsule":
  # tests a collision between a distant sphere and capsule.
  # the resulting squared distance will be an integer bigger than i32 or i64.
  let s1 = hitSphere(center = vec3Q(150, 0, 0), radius = newFix(10000))
  let c1 = hitCapsule(center1 = vec3Q(2, 0, 0), center2 = vec3Q(2, 5, 0), radius = newFix(100))
  let s2 = hitSphere(center = vec3Q(10000, 10, 0), radius = newFix(10000))
  let c2 = hitSphere(center = vec3Q(-10000, -10, 0), radius = newFix(10000))

  check isColliding(s1, s2) == true
  check isColliding(s3, s4) == false
