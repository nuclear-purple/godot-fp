# tests for fixed point vectors

import unittest
import godotfp / [fpn, vector3q]
# ~import godotfp / godot / vector3

test "normalize":
  let a = newFix(1)
  let b = newFix(0)
  let v1 = vec3Q(a, b, b)
  check v1.isNormalized == true
  var v2 = vec3Q(a, a, a)
  check v2.isNormalized == false
  v2.normalize
  check v2.isNormalized == true

test "dollar":
  let fp = newFix(0b11111, 2)
  check $fp == "7.75"
  let v = vec3Q(fp, fp, fp)
  check $v == "(7.75, 7.75, 7.75)"

test "index":
  let fp = newFix(0b11111, 2)
  var v = vec3Q(fp, fp, fp)
  check v[0] == fp
  v[1] = fp.one
  check v[1] == fp.one
  v[2] -= newFix(0b11, 2)
  check v[2] == newFix(7)

test "distance":
  let vecA = vec3Q(1, 1, 1)
  let vecB = vec3Q(3, 3, 1)
  var dist = newFix()
  dist.fromFloat(2.82842712475)
  check distanceTo(vecA, vecB).isEqualApprox(dist)

test "lerp":
  let vecA = vec3Q(1, 1, 1)
  let vecB = vec3Q(3, 3, 3)
  check lerp(vecA, vecB, newFix(1, 1)) == vec3Q(2, 2, 2)

test "stepify":
  let x = newFix(50)
  let step = newFix(0b11111, 1)
  check stepify(x, step).toBiggestFloat == 46.5

  var v = vec3Q(100, 100, 100)
  v.snap(vec3Q(3, 3, 3))
  check v == vec3Q(99, 99, 99)

test "movement":
  let v = vec3Q(2, 3, 4)
  let xy = vec3Q(1, 1, 0).normalized
  let vS = v.slide(xy)
  check vS.x.isEqualApprox(fromSameType(v.x, -1, 1))
  check vS.y.isEqualApprox(fromSameType(v.x, 1, 1))
  check vS.z.isEqualApprox(v.z)
  echo vS

#[
  test "movement float":
  let v = vec3(2.0, 3.0, 4.0)
  let xy = vec3(1.0, 1.0, 0.0).normalized
  let vS = v.slide(xy)
  echo vS
]#

test "mul":
  var a = newFix()
  var b = newFix()
  a.fromFloat(0.99)
  b.fromFloat(0.99 * 0.99)
  check (a * a).isEqualApprox(b)

  a.fromFloat(-0.8)
  b.fromFloat(0.64)
  check square(a) == b

  check mul(a, a) == b

test "sqrt":
  var a = newFix(64)
  check sqrt(a) == newFix(8)

test "fromString":
  const a = newFix()
  check fromString(a, "-100.25000").toFloat == -100.25
