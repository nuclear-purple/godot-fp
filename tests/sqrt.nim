# lut.nim

import godotfp/fpn
import unittest, times
import std/monotimes

test "sqrt":
  var a: fixedPoint32[16]
  a.fromFloat(0.2)
  echo sqrt(a)
  echo ilog2(a)

test "benchmark sqrt":
  var a, b: fixedPoint32[16]

  let time1 = getMonoTime()
  for i in countup(1, 10000):
    b = sqrt(a)
    a.setData(a.rawData + 1)
  let time2 = getMonoTime()
  let timeSqrt = time2 - time1
  echo timeSqrt
