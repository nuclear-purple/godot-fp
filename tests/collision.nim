# testCollision

import godotfp, unittest, times, std/monotimes

test "sphere-sphere":
  let a = hitSphere(center = vec3Q(1, 0, 0), radius = newFix(1))
  let b = hitSphere(center = vec3Q(3, 0, 0), radius = newFix(1))
  check isColliding(a, b) == false

  let c = hitSphere(center = vec3Q(0, 1, 0), radius = newFix(1))
  check isColliding(a, c) == true

test "sphere-capsule":
  let s1 = hitSphere(center = vec3Q(1, 1, 1), radius = newFix(10))
  let s2 = hitSphere(center = vec3Q(1, 1, 1), radius = newFix(11))
  let c = hitCapsule(center1 = vec3Q(21, -5, 1), center2 = vec3Q(21, 7, 1), radius = newFix(10))
  check isColliding(s1, c) == false
  check isColliding(s2, c) == true

test "sphere-cylinder":
  let s1 = hitSphere(center = vec3Q(0, 2, 0), radius = newFix(1))
  var s2 = s1
  s2.radius.setData(s1.radius.rawData + 1)
  let s3 = hitSphere(center = vec3Q(2, 0, 0), radius = newFix(1))
  var s4 = s3
  s4.radius.setData(s4.radius.rawData + 1)
  let s5 = hitSphere(center = vec3Q(2, 2, 0), radius = newFix(3, 1))
  let c1 = hitCylinder(base = vec3Q(0, -1, 0), radius = newFix(1), height = newFix(2))
  check not c1.isColliding(s1)
  check c1.isColliding(s2)
  check not c1.isColliding(s3)
  check c1.isColliding(s4)
  check c1.isColliding(s5)

test "capsule-capsule":
  let c1 = hitCapsule(center1 = vec3Q(5, 0, -1), center2 = vec3Q(-5, 0, -1), radius = newFix(1))
  let c2 = hitCapsule(center1 = vec3Q(7, -5, 1), center2 = vec3Q(-7, 5, 1), radius = newFix(1))
  let c3 = hitCapsule(center1 = vec3Q(7, -5, 0), center2 = vec3Q(-7, 5, 1), radius = newFix(1))
  # echo segSegDistance(c1.center1, c1.center2, c2.center1, c2.center2)
  check isColliding(c1, c2) == false
  check isColliding(c1, c3) == true

test "capsule-cylinder":
    let cyl1 = hitCylinder(base = vec3Q(0, 1, 0), radius = newFix(1), height = newFix(2))
    let cap1 = hitCapsule(center1 = vec3Q(5, 0, -1), center2 = vec3Q(-5, 0, -1), radius = newFix(1))
    let cap2 = hitCapsule(center1 = vec3Q(0, 1, 0), center2 = vec3Q(-5, 0, -1), radius = newFix(1))
    check not isColliding(cyl1, cap1)
    check iscolliding(cyl1, cap2)

test "cylinder-cylinder":
  let cyl1 = hitCylinder(base = vec3Q(0, 1, 0), radius = newFix(1), height = newFix(1))
  let cyl2 = hitCylinder(base = vec3Q(0, 0, 0), radius = newFix(1), height = newFix(1))
  let cyl3 = hitCylinder(base = vec3Q(2, 1, 0), radius = newFix(1), height = newFix(1))
  let cyl4 = hitCylinder(base = vec3Q(1, 1, 1), radius = newFix(1), height = newFix(1))
  check not isColliding(cyl1, cyl2)
  check not isColliding(cyl1, cyl3)
  check isColliding(cyl1, cyl4)

test "sphere-rect":
  let s1 = hitSphere(center = vec3Q(1, 10, 1), radius = newFix(10))
  let s2 = hitSphere(center = vec3Q(1, 10, 1), radius = newFix(11))
  let s3 = hitSphere(center = vec3Q(-4, 0, -5), radius = newFix(5))
  let s4 = hitSphere(center = vec3Q(-4, 0, -4), radius = newFix(5))
  let r1 = rectCollider(P0=vec3Q(-1, 0, -1), e1=vec3Q(2, 0, 0), e2=vec3Q(3, 0, 0))

  check isColliding(s1, r1) == false
  check isColliding(s2, r1) == true
  check isColliding(s3, r1) == false
  check isColliding(s4, r1) == true

test "sphere-box":
  discard

test "capsule-box":
  discard

test "cylinder-box":
  var Pi = newFix()
  Pi = Pi.pi
  let box = boxCollider(center = vec3Q(0, 0, 0), hWidth = vec3Q(2, 2, 2), yRotation=Pi/4)
  let cyl1 = hitCylinder(base = vec3Q(0, 2, 0), radius = newFix(1), height = newFix(1))
  let cyl2 = hitCylinder(base = vec3Q(0, 1, 0), radius = newFix(1), height = newFix(1))
  let cyl3 = hitCylinder(base = vec3Q(5, 1, 0), radius = newFix(1), height = newFix(1))
  let cyl4 = hitCylinder(base = vec3Q(2, 1, 2), radius = newFix(1), height = newFix(1))
  check not isColliding(box, cyl1)
  check isColliding(box, cyl2)
  check not isColliding(box, cyl3)
  check isColliding(box, cyl4)

test "benchmark":
  const n = 30 # number of colliders to test
  var
    spheres, capsules, cylinders, mix: array[n, HitShape]
    rects, boxes: array[n, StageCollider]
    tspcp1, tspcp2, tspsp1, tspsp2, tcpcp1, tcpcp2, tmix1, tmix2, tsppl1, tsppl2, tsbox1, tsbox2: MonoTime
    timeSpCp, timeSpSp, timeCpCp, timeMix, timeSphereRect, timeSphereBox: Duration
    test: bool
    x, y, z: FixedPointN

  # Define the colliders
  for i in 0 ..< n:
    x = newFix(i, 5) * (i mod 10)
    if x.isZero: x = x.one
    y = newFix(i, 6) * (i mod 7)
    if y.isZero: y = y.one
    z = newFix(i, 7) * (i mod 5)
    if z.isZero: z = z.one
    spheres[i] = hitSphere(center = vec3Q(x, y, z), radius = newFix(1, 6))
    capsules[i] = hitCapsule(center1 = vec3Q(x, y, z), center2 = vec3Q(z - 5, y - 5, x - 5), radius = newFix(1, 3))
    cylinders[i] = hitCylinder(radius = newFix(1, 2), base = vec3Q(x, y, z), height = newFix(1))
    rects[i] = StageCollider(kind: RectangleShape, origin: vec3Q(i, i-1, i-2), e1: vec3Q(1, 0, 0), e2: vec3Q(0, 0, 1))
    boxes[i] = StageCollider(kind: BoxShape, origin: vec3Q(x, y, z), hWidth: vec3Q(1, 1, 1),  yRotation: newFix(i)/100)
    mix[i] = if i mod 2 == 0: spheres[i] else: capsules[i]

  tspsp1 = getMonoTime()
  for i in  0 ..< n:
    for j in 0 ..< n:
      test = isColliding(spheres[i], spheres[j])
  tspsp2 = getMonoTime()
  timeSpSp = tspsp2 - tspsp1

  tspcp1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(spheres[i], capsules[j])
  tspcp2 = getMonoTime()
  timeSpCp = tspcp2 - tspcp1

  tcpcp1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(capsules[i], capsules[j])
  tcpcp2 = getMonoTime()
  timeCpCp = tcpcp2 - tcpcp1

  tmix1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(mix[i], mix[j])
  tmix2 = getMonoTime()
  timeMix = tmix2 - tmix1

  tsppl1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(spheres[i], rects[j])
  tsppl2 = getMonoTime()
  timeSphereRect = tsppl2 - tsppl1

  tsbox1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(spheres[i], boxes[j])
  tsbox2 = getMonoTime()
  timeSphereBox = tsbox2 - tsbox1

  let tcylinders1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(cylinders[i], cylinders[j])
  let tcylinders2 = getMonoTime()
  let timeCylinders = tcylinders2 - tcylinders1

  let tcylsph1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(cylinders[i], spheres[j])
  let tcylsph2 = getMonoTime()
  let timeCylSph = tcylsph2 - tcylsph1

  let tcylcap1 = getMonoTime()
  for i in 0 ..< n:
    for j in 0 ..< n:
      test = isColliding(cylinders[i], capsules[j])
  let tcylcap2 = getMonoTime()
  let timeCylCap = tcylcap2 - tcylcap1

  echo "sphere-capsule   : " & $timeSpCp
  echo "sphere-sphere    : " & $timeSpSp
  echo "capsule-capsule  : " & $timeCpCp
  echo "sphere-rect      : " & $timeSphereRect
  echo "sphere-box       : " & $timeSphereBox
  echo "mixed hitboxes   : " & $timeMix

  echo "cylinders        : " & $timeCylinders
  echo "cylinder-sphere  : " & $timeCylSph
  echo "cylinder-capsule : " & $timeCylCap

